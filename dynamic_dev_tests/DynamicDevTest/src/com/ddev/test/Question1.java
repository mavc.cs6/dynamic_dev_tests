package com.ddev.test;

import java.util.regex.Pattern;

public class Question1 {	
	
	public void decode (String text) {		
		try {
			String bar = "";		
			String regexLower = "[a-z]";
			String regexUpper = "[A-Z]";
			for (char c : text.toCharArray()) {
				bar += Pattern.matches(regexLower, String.valueOf(c))
						? Character.toString((char) (((c - 'a' + 1) % 26) + 'a'))
						: Pattern.matches(regexUpper, String.valueOf(c))
								? Character.toString((char) (((c - 'A' + 1) % 26) + 'A'))
								: c;
			}
			System.out.println(bar);
		}catch (Exception e) {
			System.out.println("Input invalid");
		}		
	}
}
