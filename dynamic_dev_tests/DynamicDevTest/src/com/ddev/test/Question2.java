package com.ddev.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class Question2 {	
	
	public void printArray (Integer[] numbers) {
		String regex ="^[1-9]\\d*$";
		try {
			for(int n: numbers) {
				if(!Pattern.matches(regex, String.valueOf(n+""))) {
					System.out.println("Input invalid");
					return;
				}
			}			
			System.out.println(Arrays.toString(fillMissingNumbers(numbers)));}
		catch (Exception e) {
			System.out.println("Input invalid");
		}
		
	}
	
	private Integer[] fillMissingNumbers(Integer[] numbers)
	{
		
		List<Integer> arrListNumbers = new ArrayList<Integer>(Arrays.asList(numbers));
		Arrays.sort(numbers);
		int i = 1;
		for (int number : numbers) {
			while (i < number) {
				arrListNumbers.add(i);
				i++;
			}
			i++;
		}
		numbers = arrListNumbers.toArray(numbers);		
		Arrays.sort(numbers);
		return numbers;
    }

}
