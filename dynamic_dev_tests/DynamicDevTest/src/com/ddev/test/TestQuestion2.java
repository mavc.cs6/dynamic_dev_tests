package com.ddev.test;

public class TestQuestion2 {

	public static void main(String[] args) {
		Question2 q2 = new Question2();		
		q2.printArray(new Integer[] {2, 1, 4, 5});
		q2.printArray(new Integer[] {4,2,9});
		q2.printArray(new Integer[] {58,60,55});
		q2.printArray(new Integer[] {-58,60,55});
	}

}
